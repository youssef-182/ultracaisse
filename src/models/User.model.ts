import IUser from "@/interfaces/IUser";
import DataManager from "@/utils/DataManager";
import {v4 as uuidv4} from 'uuid';

export default class User implements IUser {
	_id: string;
	name: string;
	private _password: number;
	avatar: string;
	isAdmin: boolean;
	
	constructor(name: string, password: number) {
		this.name = name;
		this._password = password;
	}
	
	public get password(): number {
		return this._password;
	}
	
	public set password(psswd: number) {
		this._password = psswd;
	}

	public static getUserJSON(): any {
		return DataManager.getJSONContent('Users.json');
	}

	public static getCurrentUser(): string | null {
		return JSON.parse(sessionStorage.getItem('currentUser') as string);
	}

	public static setCurrentUser(currentUser: string): void {
		return sessionStorage.setItem('currentUser', JSON.stringify(currentUser, null, '\t'));
	}

	public static createUser(name: string, password: number, avatar: string, isAdmin: boolean) {
		const usersList: Array<IUser> = this.getUserJSON();
		const value = name.toUpperCase();
		usersList.push({_id:uuidv4(),name,password,avatar,isAdmin,value});
		DataManager.setJSONContent('Users.json', usersList);
	}

	public handleLogin(usersList: []): undefined {
		const currentUser = usersList.find((user: any) => user.name.toLowerCase() === this.name.toLowerCase() && parseInt(user.password) == this.password);
		return currentUser;
	}
}