import ICategory from "@/interfaces/ICategory";
import DataManager from "@/utils/DataManager";
import {v4 as uuidv4} from 'uuid';

export default class Category implements ICategory {
	_id: string;
	name: string;
	cover: string;
	products: string[];
	constructor(name: string) {
		this.name = name;
	}

	public static getCategoryJSON(): any {
		return DataManager.getJSONContent('Categories.json');
	}

	public static createCategory(name: string, cover: string): void {
		const categoriesList: Array<ICategory> = this.getCategoryJSON();
		const value: string = name.toUpperCase();
		const products: string[] = [];
		categoriesList.push({_id:uuidv4(),name,cover,value,products});
		DataManager.setJSONContent('Categories.json', categoriesList);
	}
}