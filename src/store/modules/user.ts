import User from "@/models/User.model";

const state = {
	users: User.getUserJSON(),
	currentUser: User.getCurrentUser(),
	isLoggedIn: !!sessionStorage.getItem('isLoggedIn')
}
const getters = {
	USERS: (state: any) => {
		return state.users;
	},

	CURRENT_USER: (state: any) => {
		return state.currentUser;
	},

	IS_LOGGED_IN: (state: any) => {
		return state.isLoggedIn;
	}
}
const mutations = {
	SET_CURRENT_USER: (state: any, currentUser: string) => {
		return state.currentUser = currentUser;
	},

	SET_IS_LOGGED_IN: (state: any, status: string) => {
		return state.isLoggedIn = status;
	}
}
const actions = {
	HANDLE_LOGIN: ({commit}, {currentUser,status}) => {
		commit('SET_CURRENT_USER', currentUser);
		commit('SET_IS_LOGGED_IN', status);
		sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
		sessionStorage.setItem('isLoggedIn', status);
	}
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}