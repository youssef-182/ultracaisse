import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Login from '../views/Login.vue';
import user from '../store/modules/user';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    meta: {requiresLogin:true}
  },
  {
    path: '/createUser',
    name: 'Create User',
    component: () => import('../views/CreateUser.vue'),
    meta: {requiresLogin:true}
  },
  {
    path: '/createCategory',
    name: 'Create Category',
    component: () => import('../views/CreateCategory.vue'),
    meta: {requiresLogin:true}
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to,from,next) => {
  if(to.matched.some(record => record.meta.requiresLogin) && !user.state.isLoggedIn) next('/');
  else next();
});

export default router;