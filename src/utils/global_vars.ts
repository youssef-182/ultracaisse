import {join} from 'path';
const remote = require('@electron/remote');

const DOCUMENTS_FOLDER = remote.app.getPath('documents');
const APP_DATA_PATH = join(DOCUMENTS_FOLDER, 'miilkyy-ultracaisse/DATA');  

export {
	DOCUMENTS_FOLDER,
	APP_DATA_PATH
}