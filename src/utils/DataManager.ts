import { createReadStream, createWriteStream, existsSync, mkdir, mkdirSync, readFileSync, ReadStream, stat, WriteStream } from 'fs';
import path, {join} from 'path';
import {APP_DATA_PATH} from './global_vars';
import { DefaultData } from './DefaultData';

export default class DataManager {
	public static createDirectoryIfNotExistsSync(directory_path: string): void {
		console.info(`CHECKING IF ${directory_path} EXISTS...`);
		if(!existsSync(directory_path)) {
		console.info(`CREATING ${directory_path}...`);
			mkdir(directory_path, {recursive:true}, (err,path) => {
				if(err) throw err;
				console.info(`${path} HAS BEEN CREATED`);
			});
		} else {
			console.info(`${directory_path} ALREADY EXISTS!`);
		}
	}

	public static createDirectoryIfNotExists(directory_path: string): void {
		console.info(`CHECKING IF ${directory_path} EXISTS...`);
		if(!existsSync(directory_path)) {
			console.info(`CREATING ${directory_path}...`);
			mkdirSync(directory_path, {recursive:true});
			console.info(`${directory_path} HAS BEEN CREATED`);
		} else {
			console.info(`${directory_path} ALREADY EXISTS!`);
		}
	}

	public static setJSONContent(file_name: string, content: JSON | any): void {
		const fullPath: string = join(APP_DATA_PATH, file_name);
		const contentStr: string = JSON.stringify(content, null, '\t');
		const writeStream: WriteStream = createWriteStream(fullPath,{encoding:'utf-8'});
		writeStream.write(contentStr);
		writeStream
			.on('error', err => {
				writeStream.close();
				throw err;
			})
			.on('finish', () => console.info(`FINISHED WRITING CONTENT TO JSON FILE.`));
	}

	public static getFileContent(file_name: string): string {
		const fullPath: string = join(APP_DATA_PATH, file_name);
		const content = readFileSync(fullPath,{encoding:'utf-8'});
		return content;
		// const readStream: ReadStream = createReadStream(fullPath,{encoding:'utf-8'});
		// var data: string | Buffer = '';
		// readStream
		// 	.on('data', (chunk: string | Buffer) => {
		// 		data = chunk;
		// 	})
		// 	.on('error', err => {
		// 		throw err;
		// 	})
		// 	.on('end', () => {
		// 		return JSON.parse(data as string);
		// 	});
	}

	public static getJSONContent(file_name: string) {
		return JSON.parse(this.getFileContent(file_name));
	}

	public static createDefaultFiles(): void {
		Object.entries(DefaultData).forEach(([key,content]) => {
			const fileName = key.concat('.json');
			stat(join(APP_DATA_PATH, fileName), (err, stat) => {
				if(err == null) return console.info(`DEFAULT FILES ALREADY EXIST`);
				else if(err.code == 'ENOENT') {
					return this.setJSONContent(fileName,content);
				} else {
					console.error(`AN ERROR HAS OCCURED ${err.code}:${err.message}`);
				}
			});
		});
	}

	public static generateDefaultFilesAndDirectories(): void {
		this.createDirectoryIfNotExists(APP_DATA_PATH);
		this.createDefaultFiles();
	}

	public static test(): void {
		// const directory = join(APP_DATA_PATH, directory_name);
		// this.generateDefaultFilesAndDirectories();
		return this.getJSONContent('Users.json');
	}
}