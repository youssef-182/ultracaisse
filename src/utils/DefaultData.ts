import {v4 as uuidv4} from 'uuid';

const DefaultData: Object = {
	Users: [
		{
			_id: uuidv4(),
			name: 'Admin',
			password: 1407,
			isAdmin: true,
			value: 'ADMIN',
			avatar: `https://ui-avatars.com/api/?name=`
		}
	],
	Categories: []
}

export {DefaultData};