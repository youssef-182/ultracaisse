export default interface ICategory {
	_id: string;
	name: string;
	cover: string;
	products: string[];
	value?: string;
	createCategory?:(name: string, cover: string) => void;
}