export default interface IUser {
	_id: string;
	name: string;
	password: number;
	avatar: string;
	isAdmin: boolean;
	value?: string;
	handleLogin?:(usersList: []) => void;
}